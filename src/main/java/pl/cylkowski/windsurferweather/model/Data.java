package pl.cylkowski.windsurferweather.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@lombok.Data
public class Data {

    @JsonProperty("max_temp")
    protected double maxTemperature;
    @JsonProperty("min_temp")
    protected double minTemperature;
    @JsonProperty("wind_spd")
    protected double windSpeed;
    @JsonProperty("valid_date")
    protected String date;

}