package pl.cylkowski.windsurferweather.model;

import lombok.EqualsAndHashCode;

@lombok.Data
@EqualsAndHashCode(callSuper = true)
public class BestWeather extends Data {

    private String city;
    private String date;

    public BestWeather(String cityName, Data data) {
        this.city = cityName;
        this.date = data.getDate();
        this.windSpeed = data.getWindSpeed();
        this.maxTemperature = data.getMaxTemperature();
        this.minTemperature = data.getMinTemperature();
    }


    public BestWeather(Data data) {
        this.city = "Brak możliwości do surfowania";
        this.date = data.getDate();
        this.windSpeed = data.getWindSpeed();
        this.maxTemperature = data.getMaxTemperature();
        this.minTemperature = data.getMinTemperature();
    }
}