package pl.cylkowski.windsurferweather.restClient;

import pl.cylkowski.windsurferweather.model.Weather;
import pl.cylkowski.windsurferweather.utils.WeatherConstant;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static pl.cylkowski.windsurferweather.utils.WeatherConstant.API_URL;

@Service
public class WeatherClient {

    private final RestTemplate restTemplate = new RestTemplate();

    public Weather getForecastWeather(String city, String country, String date) {
        String url = String.format(API_URL, city, country, date, WeatherConstant.API_KEY);
        return restTemplate.getForObject(url, Weather.class);
    }
}