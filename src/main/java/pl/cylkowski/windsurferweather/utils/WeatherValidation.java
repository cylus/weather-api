package pl.cylkowski.windsurferweather.utils;

import pl.cylkowski.windsurferweather.model.Data;

import static pl.cylkowski.windsurferweather.utils.WeatherConstant.*;

public class WeatherValidation {

    public static boolean validateData(Data data) {
        double windSpeed = data.getWindSpeed();
        return windSpeed > MAX_WIND || windSpeed < MIN_WIND || data.getMaxTemperature() > MAX_TEMP || data.getMinTemperature() < MIN_TEMP;
    }
}