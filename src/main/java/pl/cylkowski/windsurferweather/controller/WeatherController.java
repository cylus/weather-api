package pl.cylkowski.windsurferweather.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cylkowski.windsurferweather.service.WeatherService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/surfing-places")
public class WeatherController {

    private final WeatherService service;

    @GetMapping("/{date}")
    public ResponseEntity<?> getWeather(@PathVariable String date) {
        return new ResponseEntity<>(service.collectBestPlaceToSurfForEachDay(date), HttpStatus.OK);
    }

}