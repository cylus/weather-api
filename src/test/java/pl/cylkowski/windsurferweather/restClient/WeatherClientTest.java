package pl.cylkowski.windsurferweather.restClient;

import pl.cylkowski.windsurferweather.model.Weather;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static pl.cylkowski.windsurferweather.utils.Tools.getToday;

@SpringBootTest
class WeatherClientTest {

    @Autowired
    WeatherClient client;

    @Test
    void getForecastWeatherTest() {
        Weather forecastWeather = client.getForecastWeather("Jastarnia", "pl", getToday());
        Assertions.assertThat(forecastWeather).isNotNull();
    }

}