package pl.cylkowski.windsurferweather.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.cylkowski.windsurferweather.model.Weather;

import java.util.List;

import static pl.cylkowski.windsurferweather.utils.Tools.getToday;
import static pl.cylkowski.windsurferweather.utils.WeatherConstant.createLocations;

@SpringBootTest
class WeatherServiceTest {

    @Autowired
    WeatherService service;

    @Test
    void getWeatherForAllCountries() {
        List<Weather> weatherForAllCountries = service.getWeatherForAllCountries(getToday());
        Assertions.assertThat(weatherForAllCountries.size()).isEqualTo(createLocations().size());
    }
}